import logging
import logging.handlers

def handle_exception():
    def decorator(func):
        def wrapper(*args, **kwargs):
            logger = setup_logger('InvoiceGenerator/Log/trace.log')
            try:
                return func(*args, **kwargs)
            except Exception as e:
                logger.info(f"Error occurred: {str(e)}")
                print(f"Error occurred: {str(e)}")
        return wrapper
    return decorator

def setup_logger(log_file):
    # Create a logger object
    logger = logging.getLogger('custom_logger')
    logger.setLevel(logging.DEBUG)

    # Create a file handler and set its level to DEBUG
    file_handler = logging.handlers.RotatingFileHandler(log_file, maxBytes=1048576, backupCount=3)
    file_handler.setLevel(logging.DEBUG)

    # Create a formatter and set it for the file handler
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(formatter)

    # Add the file handler to the logger
    logger.addHandler(file_handler)

    return logger
